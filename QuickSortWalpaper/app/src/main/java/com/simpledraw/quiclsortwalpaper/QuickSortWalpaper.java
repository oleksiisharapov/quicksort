package com.simpledraw.quiclsortwalpaper;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.SurfaceHolder;

import java.util.Random;



public class QuickSortWalpaper extends WallpaperService {

    private final Handler handler = new Handler();

    @Override
    public Engine onCreateEngine() {
        return new QuickSortEngine();
    }


    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public class QuickSortEngine extends Engine {

        private Paint p;
        private int arr[] = new int[60];

        private Canvas canvas = null;
        private int width;
        private int height;
        private float delta;
        private boolean mVisible;
        private int red;
        private int green;
        private int blue;
        private Random r;

        public QuickSortEngine() {

            p = new Paint();
            r = new Random();
            for (int i = 0; i < arr.length; i++) {
                arr[i] = r.nextInt(80);
            }
            red = r.nextInt(120);
            green = r.nextInt(120);
            blue = r.nextInt(255);
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            mVisible = visible;
            if (visible) {
                drawFrame();
            }
        }

        @Override
        public void onCreate(SurfaceHolder surfaceHolder) {

            super.onCreate(surfaceHolder);
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            this.width = width;
            this.height = height;
            super.onSurfaceChanged(holder, format, width, height);
        }

        private final Runnable drawSort = new Runnable() {
            @Override
            public void run() {
                try {
                    quickSort(0, arr.length - 1, arr);

                    Random r = new Random();
                    for (int i = 0; i < arr.length; i++) {
                        arr[i] = r.nextInt(80);
                    }
                    red = r.nextInt(120);
                    green = r.nextInt(120);
                    blue = r.nextInt(255);
                    bubbleSort(arr);
                    for (int i = 0; i < arr.length; i++) {
                        arr[i] = r.nextInt(80);
                    }
                    selectionSort(arr);
                    for (int i = 0; i < arr.length; i++) {
                        arr[i] = r.nextInt(80);
                    }

                } catch (InterruptedException ie) {
                    Log.e("QuicksortException", ie.getMessage());
                }
            }
        };

        private void drawFrame() {
            final SurfaceHolder holder = getSurfaceHolder();
            try {
                canvas = holder.lockCanvas();
                if (canvas != null) {
                    drawScene(canvas);
                }
            } finally {
                if (canvas != null) {
                    holder.unlockCanvasAndPost(canvas);
                }
            }
            if (mVisible) {
                handler.postDelayed(drawSort, 30);
            }
        }

        private void drawScene(Canvas c) {
            width = c.getWidth();
            height = c.getHeight();
            delta = (float) width / arr.length;
            Rect clear = new Rect(0, 0, width, height);
            p.setColor(Color.BLACK);
            c.drawRect(clear, p);
            p.setStrokeWidth(delta);
            for (int i = 0; i < arr.length; i++) {
                p.setColor(Color.rgb(red + arr[i], green + arr[i], blue));
                c.drawLine(i * delta, height / 2, i * delta, height / 2 - arr[i] * 4, p);
            }
        }

        private void quickSort(int left, int right, int array[]) throws InterruptedException {
            int l = left;
            int r = right;
            int base = array[(l + r) / 2];
            while (r >= l) {
                while (array[l] < base) {
                    l++;
                }
                while (array[r] > base) {
                    r--;
                }
                if (l <= r) {
                    int temp = array[r];
                    array[r] = array[l];
                    array[l] = temp;
                    l++;
                    r--;
                    drawFrame();
                    Thread.sleep(40);
                }

            }
            if (r > left) {
                quickSort(left, r, array);
            }
            if (right > l) {
                quickSort(l, right, array);
            }
        }

        private void bubbleSort(int[] array) throws InterruptedException {
            int temp;
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < array.length - i - 1; j++) {
                    if (array[j] > array[j + 1]) {
                        temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;
                        drawFrame();
                        Thread.sleep(10);
                    }
                }

            }
        }

        private void selectionSort(int[] array) throws InterruptedException {
            int min;
            int temp;
            for (int i = 0; i < array.length - 1; i++) {
                min = i;
                for (int j = i + 1; j < array.length; j++) {
                    if (array[j] < array[min]) {
                        min = j;
                    }
                }
                temp = array[i];
                array[i] = array[min];
                array[min] = temp;
                drawFrame();
                Thread.sleep(20);
            }
        }

    }
}
